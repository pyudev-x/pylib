pub struct Event<T> {
    pub handlers:Vec<Box<dyn Fn(&T)>>
}

impl<T> Event<T> {
    pub fn new() -> Self {
        return Event { handlers: vec![] };
    }

    pub fn subcribe<M>(&mut self, handler:M) where M:Fn(&T) + 'static {
        self.handlers.push(Box::new(handler));
    }

    pub fn emit(&self, value:T) {
        for handler in &self.handlers {
            handler(&value);
        }
    }
}

pub mod functions {
    use std::io::stdin;
    use rand::random;

    pub fn read_line(input:&str) -> String {
        eprint!("{input}");
        let mut val = String::new();
        stdin().read_line(&mut val).unwrap();
        return val.trim().to_string();
    }

    pub fn rand_int(cap:isize) -> isize {
        return (random::<isize>() % cap).abs();
    } 

    pub fn rand_float(cap:f32) -> f32 {
        return random::<f32>() * cap; 
    }
}